import pandas as pd
import matplotlib.pyplot as plt
from samri.report.roi import atlasassignment
from samri.plotting.aggregate import roi_distributions

df = atlasassignment('../data/dr_projection_tstat.nii.gz',
	lateralized=True,
	value_label='t Values',
	)

value_label = 't Values'
df = pd.DataFrame({
	'Structure': row['Structure'],
	'tissue type': row['tissue type'],
	value_label: float(value),
	}
	for i, row in df.iterrows() for value in row[value_label].split(', ')
	)
df['mean'] = df.groupby('Structure')[value_label].transform('mean')
###df = df.drop(columns=['Unnamed: 0.1'])
###df.drop_duplicates(keep=False, subset=['Structure'])
df = df.drop_duplicates(keep='first')

reference = df.loc[df['Structure']=='medial lemniscus/medial longitudinal fasciculus','mean'].item()
df = df.sort_values(['mean'],ascending=False)
print(reference)
df['ratio to reference'] = df['mean'] / reference
df.to_csv('../data/dr_features.csv')
