import os
import pandas as pd
import matplotlib.pyplot as plt
from samri.fetch.local import prepare_abi_connectivity_maps, prepare_feature_map
from samri.fetch.model import abi_connectivity_map
from samri.utilities import bids_autofind_df
from samri.report.roi import atlasassignment
from samri.plotting.aggregate import roi_distributions

scratch_dir = '~/.scratch/msrwba'

# Create normalized features

## ABI connectivity
#target_path_template = '~/.scratch/opfvta/features_normalized/sub-{experiment}/ses-1/anat/sub-{experiment}_ses-1_cope.nii.gz'
#target_path_template = os.path.abspath(os.path.expanduser(target_path_template))
#prepare_abi_connectivity_maps('ventral_tegmental_area',
#	reposit_path=target_path_template,
#	scaling='normalize',
#	)

# Compute cumulative map for all projections
abi_connectivity_map('dorsal_nucleus_raphe',
	save_as_zstat='../data/dr_projection_zstat.nii.gz',
	save_as_tstat='../data/dr_projection_tstat.nii.gz',
	save_as_cope='../data/dr_projection_cope.nii.gz',
	)

df = atlasassignment('../data/dr_projection_tstat.nii.gz',
	lateralized=True,
	value_label='t Values',
	)
df.to_csv('../data/dr_projections_distributions.csv')

value_label = 't Values'
df = pd.DataFrame({
	'Structure': row['Structure'],
	'tissue type': row['tissue type'],
	value_label: float(value),
	}
	for i, row in df.iterrows() for value in row[value_label].split(', ')
	)
df['mean'] = df.groupby('Structure')[value_label].transform('mean')
df = df.drop(columns=['t Values'])
df = df.drop_duplicates(keep='first')

reference = df.loc[df['Structure']=='medial lemniscus/medial longitudinal fasciculus','mean'].item()
df = df.sort_values(['mean'],ascending=False)
df['ratio to reference'] = df['mean'] / reference
df.to_csv('../data/dr_projections_referenceratio.csv')
